from collections import defaultdict
import csv
from datetime import timedelta
import json

from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import plotly.graph_objs as go


def plot_benchmarks(benchmarks, title, filename):
    data = []

    for benchmark in benchmarks:
        x = []
        y = []

        with open(benchmark['filename'], 'rb') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                hours, minutes, seconds = row['duration'].split(':')
                seconds, microseconds = seconds.split('.')
                microseconds = int(float('.{}'.format(microseconds)) * 1000000)
                td = timedelta(hours=int(hours), minutes=int(minutes), seconds=int(seconds), microseconds=int(microseconds))
                print td.total_seconds()

                x.append(int(row['tables']))
                y.append(td.total_seconds())
                print row


        trace_1 = go.Scatter(x=x, y=y, mode='markers', name='{} rows'.format(benchmark['rows']))

        totals = defaultdict(list)

        for x, y in zip(x, y):
            totals[x].append(y)

        average_x = []
        average_y = []
        for key in sorted(totals.keys()):
            average_x.append(key)
            value = totals[key]
            average_y.append(sum(value) / len(value))


        trace_2 = go.Scatter(x=average_x, y=average_y, name='{} rows, {} (Average)'.format(benchmark['rows'], benchmark['legendgroup']), legendgroup=benchmark['legendgroup'])

        data.extend([trace_2])

    layout = go.Layout(title=title, xaxis=dict(title='Tables'), yaxis=dict(title='Seconds', type='log', autorange=True))
    figure = go.Figure(data=data, layout=layout)

    with open('{}.json'.format(filename), 'w') as outfile:
        json.dump(figure, outfile)

    # plot(figure, filename='{}.html'.format(filename))


benchmark_descriptions = [
    # {
    #     'max_tables': 50,
    #     'max_max_rows': 1000000,
    #     'max_id': 10,
    #     'create_indexes': False,
    # },
    {
        'max_tables': 50,
        'max_max_rows': 1000000,
        'max_id': 10,
        'create_indexes': True,
    },
    # {
    #     'max_tables': 200,
    #     'max_max_rows': 1000,
    #     'max_id': 10,
    #     'create_indexes': False,
    # },
    # {
    #     'max_tables': 200,
    #     'max_max_rows': 100000,
    #     'max_id': 10,
    #     'create_indexes': True,
    # },
]


benchmarks = []


for bd in benchmark_descriptions:
    max_rows = 10
    if bd['create_indexes']:
        legendgroup = 'indexes'
    else:
        legendgroup = 'no-indexes'

    while max_rows <= bd['max_max_rows']:
        benchmarks.append({
            'filename': '/home/brian/angryjoin/benchmark_results/db.m4.large_max_tables_{}_rows_{}_max_id_{}_create_indexes_{}.csv'.format(bd['max_tables'], max_rows, bd['max_id'], bd['create_indexes']),
            'rows': max_rows,
            'legendgroup': legendgroup,
        })

        max_rows = max_rows * 10


plot_benchmarks(benchmarks, 'db.m4.large', 'content/plots/50_tables_with_indexes')
