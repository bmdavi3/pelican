DROP FUNCTION IF EXISTS only_create_reference_tables(integer);
CREATE FUNCTION only_create_reference_tables(tables integer) RETURNS void AS $function_text$
BEGIN
    FOR i IN 1..tables LOOP
        EXECUTE 'DROP TABLE IF EXISTS reference_table_' || i || ' CASCADE;';

        RAISE NOTICE 'Creating table...';

        EXECUTE format($$
            CREATE TABLE reference_table_%1$s (
                pt_id integer unique references primary_table (id) ON DELETE CASCADE ON UPDATE CASCADE,
                value double precision
            );
        $$, i);
    END LOOP;
END;
$function_text$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS insert_into_reference_tables(integer, integer, integer, boolean);
CREATE FUNCTION insert_into_reference_tables(table_begin integer, table_end integer, rows integer, create_indexes boolean) RETURNS void AS $function_text$
BEGIN
    FOR i IN table_begin..table_end LOOP
        EXECUTE format($$
            INSERT INTO reference_table_%1$s (pt_id, value)
            SELECT
                id,
                random()
            FROM
                primary_table
            ORDER BY
                random();
        $$, i);

        IF create_indexes THEN
            RAISE NOTICE 'Creating index...';
            EXECUTE 'CREATE INDEX ON reference_table_' || i || ' (value);';
        END IF;
        RAISE NOTICE 'Done creating table and index if necessary';
    END LOOP;
END;
$function_text$ LANGUAGE plpgsql;


SELECT only_create_reference_tables(10);
