CREATE OR REPLACE FUNCTION chop_up_insert() RETURNS trigger AS $$
DECLARE
insert_text text;
new_id INT;
BEGIN

    INSERT INTO primary_table (id) VALUES (DEFAULT) RETURNING id INTO new_id;

    SELECT
        string_agg('INSERT INTO reference_table_' || gs || ' (pt_id, value) VALUES ($1, $2.value_' || gs || ');', ' ')
    INTO insert_text
    FROM
        generate_series(1, 10) AS gs;

    EXECUTE insert_text USING new_id, NEW;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION chopped_up_delete() RETURNS trigger AS $$
DECLARE
insert_text text;
new_id INT;
BEGIN

    DELETE FROM primary_table WHERE id = OLD.id;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION chop_up_update() RETURNS trigger AS $$
DECLARE
delete_text text;
insert_text text;
new_id INT;
BEGIN

    -- Strategy: Update changed reference table values, then update pk if it has changed.  UPDATE CASCADE should take
    --           care of the rest, but need to verify

    SELECT
        string_agg('DELETE FROM reference_table_' || gs || ' WHERE pt_id = $1;', ' ')
    INTO insert_text
    FROM
        generate_series(1, 10) AS gs;

    EXECUTE insert_text USING OLD.id;

    SELECT
        string_agg('INSERT INTO reference_table_' || gs || ' (pt_id, value) VALUES ($1, $2.value_' || gs || ');', ' ')
    INTO insert_text
    FROM
        generate_series(1, 10) AS gs;

    EXECUTE insert_text USING OLD.id, NEW;

    IF NEW.id IS DISTINCT FROM OLD.id THEN
        UPDATE primary_table SET id = NEW.id WHERE id = OLD.id;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;



CREATE TRIGGER chop_up_insert INSTEAD OF INSERT ON combined FOR EACH ROW EXECUTE PROCEDURE chop_up_insert();
CREATE TRIGGER chopped_up_delete INSTEAD OF DELETE ON combined FOR EACH ROW EXECUTE PROCEDURE chopped_up_delete();
CREATE TRIGGER chop_up_update INSTEAD OF UPDATE ON combined FOR EACH ROW EXECUTE PROCEDURE chop_up_update();
