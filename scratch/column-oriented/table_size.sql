SELECT
    :'table_name',
    pg_relation_size(:'table_name') AS size_bytes,
    pg_size_pretty(pg_relation_size(:'table_name')) AS size_pretty;
