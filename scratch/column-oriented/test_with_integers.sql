

DROP FUNCTION IF EXISTS create_primary_table(integer);
CREATE FUNCTION create_primary_table(rows integer) RETURNS void AS $function_text$
BEGIN

    DROP TABLE IF EXISTS primary_table CASCADE;
    CREATE TABLE primary_table (
        id serial primary key
    );

    INSERT INTO primary_table (id)
    SELECT
        nextval('primary_table_id_seq')
    FROM
        generate_series(1, rows);
END;
$function_text$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS create_reference_tables(integer, integer, boolean);
CREATE FUNCTION create_reference_tables(tables integer, rows integer, create_indexes boolean) RETURNS void AS $function_text$
BEGIN
    FOR i IN 1..tables LOOP
        EXECUTE 'DROP TABLE IF EXISTS reference_table_' || i || ' CASCADE;';

        RAISE NOTICE 'Creating and inserting into table...';

        EXECUTE format($$
            CREATE TABLE reference_table_%1$s (
                pt_id integer unique references primary_table (id),
                value double precision
            );

            INSERT INTO reference_table_%1$s (pt_id, value)
            SELECT
                id,
                random()
            FROM
                primary_table
            ORDER BY
                random();
        $$, i);

        IF create_indexes THEN
            RAISE NOTICE 'Creating index...';
            EXECUTE 'CREATE INDEX ON reference_table_' || i || ' (value);';
        END IF;
        RAISE NOTICE 'Done creating table and index if necessary';
    END LOOP;
END;
$function_text$ LANGUAGE plpgsql;


DROP FUNCTION IF EXISTS analyze_reference_tables(integer);
CREATE FUNCTION analyze_reference_tables(tables integer) RETURNS void AS $function_text$
BEGIN
    FOR i IN 1..tables LOOP
        EXECUTE 'ANALYZE reference_table_' || i || ';';
    END LOOP;
END;
$function_text$ LANGUAGE plpgsql;




DROP FUNCTION IF EXISTS create_combined_view(integer);
CREATE FUNCTION create_combined_view(reference_tables integer) RETURNS void AS $function_text$
DECLARE
    where_clause_text text;
    join_list text;
    column_select_list text;
BEGIN
    SELECT
        string_agg($$,
                rt_$$ || gs || '.value AS value_' || gs, '')
    INTO column_select_list
    FROM
        generate_series(1, reference_tables) AS gs;

    SELECT
        string_agg($$ LEFT JOIN
                reference_table_$$ || gs || ' AS rt_' || gs || $$ ON
                     pt.id = rt_$$ || gs || '.pt_id', '')
    INTO join_list
    FROM
        generate_series(1, reference_tables) AS gs;

    DROP VIEW IF EXISTS combined;
    EXECUTE format($$
        CREATE VIEW combined AS
        SELECT
            pt.id%1$s
        FROM
            primary_table AS pt%2$s;
    $$, column_select_list, join_list);
END;
$function_text$ LANGUAGE plpgsql;




-------------------------------- SINGLE TABLE -----------------------------------

DROP FUNCTION IF EXISTS create_traditional_table(integer, integer);
CREATE FUNCTION create_traditional_table(columns integer, rows integer) RETURNS void AS $function_text$
DECLARE
    column_text text;
    foreign_key_text text;
    foreign_key_column_text text := '';
    foreign_key_insert_text text := '';
BEGIN
    SELECT
        string_agg(', value_' || gs || $$ double precision default random() $$, ' ')
    INTO column_text
    FROM
        generate_series(1, columns) AS gs;

    -- Create traditional table
    RAISE NOTICE 'Creating traditional table...';
    DROP TABLE IF EXISTS traditional_table CASCADE;
    EXECUTE format($$
        CREATE TABLE traditional_table (
            id serial primary key
            %1$s
        );
    $$, column_text);

    -- Insert traditional table rows
    EXECUTE format($$
        INSERT INTO traditional_table (id)
        SELECT
            nextval('traditional_table_id_seq')
        FROM
            generate_series(1, %1$s);
    $$, rows);
END;
$function_text$ LANGUAGE plpgsql;

\set number_of_rows 100
\set number_of_tables 10


SELECT create_primary_table(:number_of_rows);
ANALYZE primary_table;
SELECT only_create_reference_tables(:number_of_tables);
SELECT insert_into_reference_tables(1, :number_of_tables, :number_of_rows, True);
SELECT analyze_reference_tables(:number_of_tables);
SELECT create_combined_view(:number_of_tables);

-- NEED TO ADJUST HARD CODED TABLE NUMBER
\i instead_of.sql

-- SELECT create_reference_tables(:number_of_tables, :number_of_rows, True);
-- SELECT analyze_reference_tables(:number_of_tables);
-- SELECT create_combined_view(:number_of_tables);
--SELECT create_traditional_table(:number_of_tables, :number_of_rows);
--ANALYZE traditional_table;


EXPLAIN (ANALYZE, BUFFERS)
SELECT
    sum(value_50)
FROM
    traditional_table;

EXPLAIN (ANALYZE, BUFFERS)
SELECT
    sum(value_5)
FROM
    combined;
