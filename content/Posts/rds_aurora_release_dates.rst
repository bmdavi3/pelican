RDS, Aurora Release Dates
################################################

:date: 2021-09-10 15:00
:tags: postgresql
:slug: rds-aurora-release-dates
:summary: RDS, Aurora Release Dates
:status: published

..
   :status: published  <- Put this above to actually post when ready

Below are the dates of Postgres major version releases and when they first became available on RDS and Aurora.

Useful for gaining a leg up in your office AWS Managed Postgres Major Version Release Date betting pool.

.. code-block:: plpgsql

        WITH releases AS (
          SELECT * FROM
          (VALUES
            ('13',  '2020-09-24'::date, '2021-02-24'::date, '2021-08-26'::date),
            ('12',  '2019-10-03'::date, '2020-03-31'::date, '2021-01-28'::date),
            ('11',  '2018-10-18'::date, '2019-03-13'::date, '2019-11-26'::date),
            ('10',  '2017-10-05'::date, '2018-02-27'::date, '2018-09-25'::date),
            ('9.6', '2016-09-29'::date, '2016-11-11'::date, '2017-10-24'::date)
          ) AS t (version, pg, rds, aurora)
        )
        SELECT
          version,
          pg AS "Postgres Release",
          rds AS "RDS Release",
          aurora AS "Aurora Release",
          rds - pg AS "PG -> RDS Lag",
          aurora - pg AS "PG -> Aurora Lag",
          aurora - rds AS "RDS -> Aurora Lag"
        FROM
          releases
        ORDER BY
          version::decimal


.. code-block:: plpgsql

         version | Postgres Release | RDS Release | Aurora Release | PG -> RDS Lag | PG -> Aurora Lag | RDS -> Aurora Lag
        ---------+------------------+-------------+----------------+---------------+------------------+-------------------
         9.6     | 2016-09-29       | 2016-11-11  | 2017-10-24     |            43 |              390 |               347
         10      | 2017-10-05       | 2018-02-27  | 2018-09-25     |           145 |              355 |               210
         11      | 2018-10-18       | 2019-03-13  | 2019-11-26     |           146 |              404 |               258
         12      | 2019-10-03       | 2020-03-31  | 2021-01-28     |           180 |              483 |               303
         13      | 2020-09-24       | 2021-02-24  | 2021-08-26     |           153 |              336 |               183
        (5 rows)


Postgres release dates
https://www.postgresql.org/support/versioning/

13 on RDS
https://aws.amazon.com/about-aws/whats-new/2021/02/amazon-rds-now-supports-postgresql-13/

13 on Aurora
https://aws.amazon.com/about-aws/whats-new/2021/08/amazon-aurora-postgresql-13/

12 on RDS
https://aws.amazon.com/about-aws/whats-new/2020/03/amazon-rds-now-supports-postgresql-12/

12 on Aurora
https://aws.amazon.com/about-aws/whats-new/2021/01/amazon-aurora-supports-postgresql-12/

11 on RDS
https://aws.amazon.com/about-aws/whats-new/2019/03/postgresql11-now-supported-in-amazon-rds/

11 on Aurora
https://aws.amazon.com/about-aws/whats-new/2019/11/amazon-aurora-postgresql-compatibility-supports-postgresql-11-4/

10 on RDS
https://aws.amazon.com/about-aws/whats-new/2018/02/postgresql-10-now-supported-in-amazon-rds/

10 on Aurora
https://aws.amazon.com/about-aws/whats-new/2018/09/amazon-aurora-postgresql-compatibility-supports-postgresql104/

9.6 on RDS
https://aws.amazon.com/about-aws/whats-new/2016/11/amazon-rds-now-supports-postgresql-9-6-1/

9.6 on Aurora
https://aws.amazon.com/blogs/aws/now-available-amazon-aurora-with-postgresql-compatibility/
