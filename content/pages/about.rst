About
#####

:slug: about
:summary: About me summary
:status: published


.. image:: {filename}/images/Brian.jpg
    :align: right
    :alt: Picture of Brian

I'm a Software Engineer / Data Engineer and PostgreSQL Spirit Animal at Lumere, where we work to help hospitals make better sense of their data.

Previously I spent a number of years as a Software Engineer / "The Database Guy" at CouponCabin.com, which serves thousands of hand verified coupons to millions of users.

I grew up in the Chicagoland area, and still live and work in Chicago today.

If you want to hit me up and talk PostgreSQL over coffee or a beer, shoot me an email at brian@brianlikespostgres.com
